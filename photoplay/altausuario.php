<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php";
if ($_SESSION['id_usuario']==null) {
header('location:index.php');
}
?>
</header>
<section id="secaltausu">
<h1>Formulario alta usuarios</h1>
<div id="guiadatos">
    <p><button id="botonalta">Mostrar</button> guía de inserción de datos. </p> 
    <p class="textoaltausu">Estimado moderador,
Para dar de alta a un nuevo usuario en la web de Photoplay, sigue estos simples pasos: <br><br>
    1. Completa los campos obligatorios del formulario, como nombre, apellido, nombre de usuario, correo electrónico, contraseña y fecha de nacimiento. Asegúrate de que la información sea precisa y completa. <br><br>
    2. Verifica la disponibilidad del nombre de usuario. Si no es válido, recibirás una notificación para modificarlo por uno disponible. <br><br>
    3. Revisa la información ingresada y asegúrate de su exactitud. <br><br>
    4. Haz clic en el botón de "Guardar" o "Registrar" para completar el proceso de alta del usuario. <br><br>
Recuerda que tu labor como moderador es esencial para brindar una experiencia satisfactoria a los usuarios de Photoplay. Si tienes alguna duda o necesitas ayuda durante el proceso de alta, no dudes en contactar al equipo de soporte técnico.
¡Gracias por tu dedicación en mantener nuestra comunidad de Photoplay en funcionamiento! Tu contribución es invaluable para ofrecer una experiencia de juego increíble.</p>
</div>
<form action="funcaltausu.php" method="post" id="formaltausu">
<div>
<label class="labelaltausu" for="">Nombre :</label>
<input class="inputalta" type="text" name="nombre" id="nombre">
</div>
<div>
<label class="labelaltausu" for="">Apellido:</label>
<input class="inputalta" type="text" name="apellido" id="apellido">
</div>
<div>
<label class="labelaltausu" for="">Usuario :</label>
<input class="inputalta" type="text" name="usuario" id="usuario"> <br>
<span id="spanusuario"></span>
</div>
<div>
<label class="labelaltausu" for="">Email :</label>
<input class="inputalta" type="email" name="email" id="email">
</div>
<div>
<label class="labelaltausu" for="password">Contraseña :</label>
<input class="inputalta" type="password" name="contraseña" id="contraseña">
</div>

<div>
<label class="labelaltausu" for="password">Confirmar contraseña :</label>
<input class="inputalta" type="password" name="recontraseña" id="contraseña2"> 
<span id="spancontraseña"></span>
</div>

<div>
<label class="labelaltausu" for="fecha">Fecha nacimiento :</label>
<input class="inputalta" type="date" name="fecha" id="fecha">
</div>
<div>
<input type="submit" value="Registrar" id="registrar">
</div>
</form>
<div id="divspan">
    <span id="altausuconfirm"> </span>
</div>
</section>
<?php
include "footer.php"
?>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
<script src="./js/altausuario.js"></script>
<script src="./js/efectos.js"></script>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php";
if ($_SESSION['id_usuario']==null) {
header('location:index.php');
}
?>
</header>

<section id="secaltacategoria">
    <h1>Formulario alta categorías</h1>
    <div>
    Antes de dar de alta una categoría, compruebe que esta no exista en la base de datos, para esto ingrese el texto en el siguiente recuadro y presione el boton comprobar, si el resultado es: <b>No existe</b> , podra registrarla, si es: <b>existe</b>, no podra registrarla. <br><br>
    <label for="">Categoría a comprobar :</label>
    <input type="text" name="comprobar" id="comprobar">
    <span id="comprocat"></span>
    <button id="comprobarcate">comprobar</button>
    <span class="spancate"></span>
    </div>
    
<form action="" method="post" enctype="multipart/form-data" id="formcate">
    <h3>Registro</h3>
<div>
<label class="labelaltausu" for="">Temática</label>
<select class="inputalta" name="tematica" id="tematica">
    
    <option value="FOL">FOL</option>
    <option value="Inglés">Inglés</option>
</select>
</div>
<div>
<label class="labelaltausu" for="">Categoría</label>
<input class="inputalta" type="text" name="categoria" id="categoria">
</div>

<div class="regicate">
<input type="submit" value="Registrar" id="registrar">
<span id="confirmcate"></span>
</div>






</form>

<div>
    <span id="catregistrada"></span>
</div>
</section>
<?php
include "footer.php"
?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>

<script src="./js/altacategoria.js"></script>
</html>
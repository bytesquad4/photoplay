<?php
 function connectBBDD()
 {
   $mysqli = new mysqli("127.0.0.1", "almi", "almi123", "photoplay");
   if($mysqli->connect_errno)
   {
     echo "Fallo en la conexión: aqui".$mysqli->connect_errno;
   }
   return $mysqli;
 }

 function insertarUsuario($nombre, $apellido, $usuario ,$email, $contraseña, $fecha, $tipo)
  {
    $mysqli = connectBBDD();
    $sql = "INSERT INTO usuario(nombre, apellido, user, email, contraseña, fecha_nacimiento, id_tipo) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("ssssssi", $nombre, $apellido, $usuario ,$email, $contraseña, $fecha, $tipo);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $mysqli->close();

    return $ejecucion;
  }

  function mostrarusuarios()
  {
    $mysqli = connectBBDD();
    $sql = "SELECT * FROM `usuario`";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }
    $id_usuario = -1;
    $nombre = "";
    $apellido = "";
    $usuario = "";
    $email = "";
    $contraseña = "";
    $fecha = "";
    $tipo = -1;

    $vincular = $sentencia->bind_result($id_usuario, $nombre, $apellido, $usuario, $email, $contraseña, $fecha, $tipo );
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }


    $usuarios = array();

    while($sentencia->fetch())
    {
      $usuario= array(
        
        'id_usuario'=> $id_usuario,
        'nombre' => $nombre,
        'apellido' => $apellido,
        'user' => $usuario,
        'email' => $email,
        'contraseña' => $contraseña,
        'fecha_nacimiento' => $fecha,
        'id_tipo' => $tipo,
      );
      $usuarios[] = $usuario;
    }

    $mysqli->close();

    return $usuarios;
  }

  function modiusu($id_usuario)
  {
    $mysqli = connectBBDD();
    $sql = "SELECT * FROM usuario WHERE id_usuario = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("i", $id_usuario);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $id_usuario = -1;
    $nombre = "";
    $apellido = "";
    $usuario = "";
    $email = "";
    $contraseña = "";
    $fecha = "";
    $tipo = -1;

    $vincular = $sentencia->bind_result($id_usuario, $nombre, $apellido, $usuario, $email, $contraseña, $fecha, $tipo);
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }

    while($sentencia->fetch())
    {
      $usuario= array(
        
        'id_usuario'=> $id_usuario,
        'nombre' => $nombre,
        'apellido' => $apellido,
        'user' => $usuario,
        'email' => $email,
        'contraseña' => $contraseña,
        'fecha_nacimiento' => $fecha,
        'id_tipo' => $tipo,
      );
    
    }

    return $usuario;
  }

  function actualizarusuario($id_usuario, $nombre ,$apellido, $email, $contraseña, $fecha)
  {
    $mysqli = connectBBDD();
    
    $sql = "UPDATE usuario
    SET nombre = ?, apellido = ?, email = ?, contraseña = ?, fecha_nacimiento = ?
    WHERE id_usuario = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("sssssi", $nombre ,$apellido, $email, $contraseña, $fecha, $id_usuario );
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $mysqli->close();

    return $ejecucion;
  }

  function eliminarusuario($id_usuario){
    $mysqli = connectBBDD();
    $sql = "DELETE FROM usuario WHERE id_usuario=?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("i",$id_usuario);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $mysqli->close();

    return $ejecucion;
  }


  function getUsuario($usuario)
  {
    $mysqli = connectBBDD();
    $sql = "SELECT user FROM usuario WHERE user = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("s", $usuario);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $usuario = "";

    $vincular = $sentencia->bind_result($usuario);
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }


    if($sentencia->fetch())
    {

    }

    $mysqli->close();

    return $usuario;
  }

// paginacion 
function mostrarUsuariosPaginados($pagina, $registrosPorPagina)
{
    $mysqli = connectBBDD();
    
    // Calcular el índice de inicio para la consulta
    $inicio = ($pagina - 1) * $registrosPorPagina;

    $sql = "SELECT * FROM `usuario` LIMIT ?, ?";
    $sentencia = $mysqli->prepare($sql);
    if (!$sentencia) {
        echo "Fallo al preparar la sentencia: " . $mysqli->errno;
    }

    // Vincular los parámetros de la consulta
    $sentencia->bind_param("ii", $inicio, $registrosPorPagina);

    $ejecucion = $sentencia->execute();
    if (!$ejecucion) {
        echo "Fallo en la ejecucion: " . $mysqli->errno;
    }

    $id_usuario = -1;
    $nombre = "";
    $apellido = "";
    $usuario = "";
    $email = "";
    $contraseña = "";
    $fecha = "";
    $tipo = -1;

    $vincular = $sentencia->bind_result($id_usuario, $nombre, $apellido, $usuario, $email, $contraseña, $fecha, $tipo);
    if (!$vincular) {
        echo "Fallo al vincular parametros: " . $mysqli->errno;
    }

    $usuarios = array();

    while ($sentencia->fetch()) {
        $usuario = array(
            'id_usuario' => $id_usuario,
            'nombre' => $nombre,
            'apellido' => $apellido,
            'user' => $usuario,
            'email' => $email,
            'contraseña' => $contraseña,
            'fecha_nacimiento' => $fecha,
            'id_tipo' => $tipo,
        );
        $usuarios[] = $usuario;
    }

    $mysqli->close();

    return $usuarios;
}

function obtenerTotalUsuarios()
{
    $mysqli = connectBBDD();

    $sql = "SELECT COUNT(*) as total FROM `usuario`";
    $resultado = $mysqli->query($sql);
    if (!$resultado) {
        echo "Fallo en la consulta: " . $mysqli->error;
    }

    $fila = $resultado->fetch_assoc();
    $total = $fila['total'];

    $mysqli->close();

    return $total;
}










































  ///////////
  function eliminarNoticia($id_noticia){
    $mysqli = connectBBDD();
    $sql = "DELETE FROM noticia WHERE id_noticia=?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("i",$id_noticia);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $mysqli->close();

    return $ejecucion;
  }

  function login($nombre, $contraseña, $id_tipo)
  {
    $mysqli = connectBBDD();
    $sql = "SELECT id_usuario FROM usuario WHERE nombre = ? AND contraseña = ? AND id_tipo = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("ssi", $nombre, $contraseña, $id_tipo);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $id_usuario = 0;

    $vincular = $sentencia->bind_result($id_usuario);
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }


    if($sentencia->fetch())
    {

    }

    $mysqli->close();

    return $id_usuario;
  }

  function insertarNoticia($titulo, $texto ,$imagen, $fecha, $resumen, $id_usuario)
  {
    $mysqli = connectBBDD();
    $sql = "INSERT INTO noticia(titulo, texto, imagen, fecha, resumen, id_usuario) VALUES (?, ?, ?, ?, ?, ?)";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("sssssi", $titulo, $texto ,$imagen, $fecha, $resumen, $id_usuario);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $mysqli->close();

    return $ejecucion;
  }

  function actualizarNoticia($titulo, $texto ,$imagen, $fecha, $resumen, $id_usuario, $id_noticia)
  {
    $mysqli = connectBBDD();
    
    $sql = "UPDATE noticia
    SET titulo = ?, texto = ?, imagen = ?, fecha = ?, resumen = ?, id_usuario = ?
    WHERE id_noticia = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("sssssii", $titulo, $texto ,$imagen, $fecha, $resumen, $id_usuario, $id_noticia);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $mysqli->close();

    return $ejecucion;
  }





  
  function getNoticia()
  {
    $mysqli = connectBBDD();

    $sql = "SELECT * FROM noticia";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo en la preparacion de la sentencia: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecución de la sentencia: ".$mysqli->errno;
    }

    $id_noticia= -1;
    $titulo = "";
    $texto = "";
    $imagen = "";
    $fecha = "";
    $resumen = "";
    $id_usu = -1;
    $vincular = $sentencia->bind_result($id_noticia, $titulo, $texto, $imagen, $fecha, $resumen, $id_usu );
    if(!$vincular)
    {
      echo "Fallo al vincular los resultados: ".$mysqli->errno;
    }

    $noti = array();

    while($sentencia->fetch())
    {
      $noticia = array(
        'id_noticia'=> $id_noticia,
        'titulo' => $titulo,
        'texto' => $texto,
        'imagen' => $imagen,
        'fecha' => $fecha,
        'resumen' => $resumen,
        'id_usuario' => $id_usu,
      );
      $noti[] = $noticia;
    }
    

    $mysqli->close();
    return $noti;
  }
  
  function get5Noticias()
  {
    $mysqli = connectBBDD();

    $sql = "SELECT id_noticia,titulo, texto, imagen, fecha, resumen, nombre, apellido FROM noticia inner join usuario u on noticia.id_usuario=u.id_usuario ORDER BY fecha DESC LIMIT 6";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo en la preparacion de la sentencia: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecución de la sentencia: ".$mysqli->errno;
    }

    $id_noticia= -1;
    $titulo = "";
    $texto = "";
    $imagen = "";
    $fecha = "";
    $resumen = "";
    $nombre = "";
    $apellido= "";
    $vincular = $sentencia->bind_result($id_noticia, $titulo, $texto, $imagen, $fecha, $resumen, $nombre, $apellido );
    if(!$vincular)
    {
      echo "Fallo al vincular los resultados: ".$mysqli->errno;
    }

    $noti = array();

    while($sentencia->fetch())
    {
      $noticia = array(
        'id_noticia'=> $id_noticia,
        'titulo' => $titulo,
        'texto' => $texto,
        'imagen' => $imagen,
        'fecha' => $fecha,
        'resumen' => $resumen,
        'nombre' => $nombre,
        'apellido' => $apellido,
      );
      $noti[] = $noticia;
    }
    

    $mysqli->close();
    return $noti;
  }

  function getNoticias($id_noticia)
  {
    $mysqli = connectBBDD();

    $sql = "SELECT id_noticia,titulo, texto, imagen, fecha, resumen, nombre, apellido FROM noticia inner join usuario u on noticia.id_usuario=u.id_usuario WHERE id_noticia=?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo en la preparacion de la sentencia: ".$mysqli->errno;
      
    }
    $asignar = $sentencia->bind_param("i", $id_noticia);
      if(!$asignar)
      {
        echo "Fallo al asignar parámetros: ".$mysqli->errno;
      }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecución de la sentencia: ".$mysqli->errno;
    }

    $id_noticia= -1;
    $titulo = "";
    $texto = "";
    $imagen = "";
    $fecha = "";
    $resumen = "";
    $nombre = "";
    $apellido= "";
    $vincular = $sentencia->bind_result($id_noticia, $titulo, $texto, $imagen, $fecha, $resumen, $nombre, $apellido );
    if(!$vincular)
    {
      echo "Fallo al vincular los resultados: ".$mysqli->errno;
    }

    

    while($sentencia->fetch())
    {
      $noticia = array(
        'id_noticia'=> $id_noticia,
        'titulo' => $titulo,
        'texto' => $texto,
        'imagen' => $imagen,
        'fecha' => $fecha,
        'resumen' => $resumen,
        'nombre' => $nombre,
        'apellido' => $apellido,
      );
      
    }
    

    $mysqli->close();
    return $noticia;
  }
  

  function getUnicNoticia($id)
  {
    $mysqli = connectBBDD();

    $sql = "SELECT * FROM noticia WHERE id_noticia=?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo en la preparacion de la sentencia: ".$mysqli->errno;
    }
    $asignar = $sentencia->bind_param("i", $id);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecución de la sentencia: ".$mysqli->errno;
    }

    $id_noticia= -1;
    $titulo = "";
    $texto = "";
    $imagen = "";
    $fecha = "";
    $resumen = "";
    $id_usu = -1;
    $vincular = $sentencia->bind_result($id_noticia, $titulo, $texto, $imagen, $fecha, $resumen, $id_usu );
    if(!$vincular)
    {
      echo "Fallo al vincular los resultados: ".$mysqli->errno;
    }

    $noti = array();

    while($sentencia->fetch())
    {
      $noticia = array(
        'id_noticia'=> $id_noticia,
        'titulo' => $titulo,
        'texto' => $texto,
        'imagen' => $imagen,
        'fecha' => $fecha,
        'resumen' => $resumen,
        'id_usuario' => $id_usu,
      );
      $noti[] = $noticia;
    }

    $mysqli->close();
    return $noti;
  }


  function getTags()
  {
    $mysqli = connectBBDD();

    $sql = "SELECT * FROM tags";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo en la preparacion de la sentencia: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecución de la sentencia: ".$mysqli->errno;
    }

    $id_tags= -1;
    $tags = "";
    
    $vincular = $sentencia->bind_result($id_tags, $tags);
    if(!$vincular)
    {
      echo "Fallo al vincular los resultados: ".$mysqli->errno;
    }

    $tagsA = array();

    while($sentencia->fetch())
    {
      $noticia = array(
        'id_tags'=> $id_tags,
        'tags' => $tags,
      );
      $tagsA[] = $noticia;
    }
    

    $mysqli->close();
    return $tagsA;
  }

  function getFecha($id)
  {
    $mysqli = connectBBDD();
    $sql = "SELECT n.fecha FROM noticias_tags inner join noticia n on noticias_tags.id_noticia=n.id_noticia WHERE noticias_tags.id_tags = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("i",  $id);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }

    $fecha = "";

    $vincular = $sentencia->bind_result($fecha);
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }


    $fechasf = array();

    while($sentencia->fetch())
    {
      $fechas= array(
        
        'fecha' => $fecha,
      );
      $fechasf[] = $fechas;
    }

    $mysqli->close();

    return $fechasf;
  }

  function getNotiselec($tags, $fecha)
  {
    $mysqli = connectBBDD();
    $sql = "select n.* from noticias_tags as nt inner join noticia n on nt.id_noticia=n.id_noticia inner join tags t on nt.id_tags=t.id_tags where t.id_tags = ? and n.fecha = ?  ";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("is",  $tags, $fecha );
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }
    $id_noticia = -1;
    $titulo = "";
    $texto = "";
    $imagen = "";
    $fecha = "";
    $resumen = "";
    $id_usu = -1;

    $vincular = $sentencia->bind_result($id_noticia, $titulo, $texto, $imagen, $fecha, $resumen, $id_usu );
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }


    $noti = array();

    while($sentencia->fetch())
    {
      $fechas= array(
        
        'id_noticia'=> $id_noticia,
        'titulo' => $titulo,
        'texto' => $texto,
        'imagen' => $imagen,
        'fecha' => $fecha,
        'resumen' => $resumen,
        'id_usuario' => $id_usu,
      );
      $noti[] = $fechas;
    }

    $mysqli->close();

    return $noti;
  }



  function mostrarNoticia($id)
  {
    $mysqli = connectBBDD();
    $sql = "SELECT * FROM `noticia` WHERE id_noticia = ?";
    $sentencia = $mysqli->prepare($sql);
    if(!$sentencia)
    {
      echo "Fallo al preparar la sentencia: ".$mysqli->errno;
    }

    $asignar = $sentencia->bind_param("i", $id);
    if(!$asignar)
    {
      echo "Fallo al asignar parámetros: ".$mysqli->errno;
    }

    $ejecucion = $sentencia->execute();
    if(!$ejecucion)
    {
      echo "Fallo en la ejecucion: ".$mysqli->errno;
    }
    $id_noticia = -1;
    $titulo = "";
    $texto = "";
    $imagen = "";
    $fecha = "";
    $resumen = "";
    $id_usu = -1;

    $vincular = $sentencia->bind_result($id_noticia, $titulo, $texto, $imagen, $fecha, $resumen, $id_usu );
    if(!$vincular)
    {
      echo "Fallo al vincular parametros: ".$mysqli->errno;
    }


    $noti = array();

    while($sentencia->fetch())
    {
      $fechas= array(
        
        'id_noticia'=> $id_noticia,
        'titulo' => $titulo,
        'texto' => $texto,
        'imagen' => $imagen,
        'fecha' => $fecha,
        'resumen' => $resumen,
        'id_usuario' => $id_usu,
      );
      $noti[] = $fechas;
    }

    $mysqli->close();

    return $noti;
  }

  

  














?>
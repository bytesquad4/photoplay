<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php";
include "bbdd.php";
if ($_SESSION['id_usuario']==null) {
header('location:index.php');
}
$id_usu = $_GET['id_usuario'];
$usuarios = modiusu($id_usu);

?>
</header>

<section id="secmodiusu">
    <h1>Modificación de usuarios</h1>
    <div id="guiadatos">
    <p><button id="botonalta">Mostrar</button> información importante antes de modificar.</p> 
    <p class="textoaltausu">Querido moderador, Al realizar modificaciones en el formulario de usuarios de Photoplay, te instamos a ser cuidadoso y respetar la privacidad de nuestros usuarios. Cada cambio que realices puede afectar su experiencia, por lo que te recordamos: <br><br>

1. Ingresa datos precisos y verídicos en cada campo. <br><br>
2. Mantén la confidencialidad de la información personal de los usuarios. <br><br>
2. Cumple con las regulaciones de protección de datos aplicables. <br><br>
4. Tu atención y cuidado contribuyen a crear una comunidad segura y confiable en Photoplay. <br><br>

¡Gracias por tu compromiso en proteger la privacidad de nuestros usuarios!</p>
</div>
<form action="funcmodiusu.php" method="post" id="formModiusu">
<div>
<label class="labelaltausu" for="">Nombre :</label>
<input class="inputalta" type="text" name="nombre" id="nombre" value="<?php echo $usuarios['nombre'] ?>">
</div>
<div>
<label class="labelaltausu" for="">Apellido :</label>
<input class="inputalta" type="text" name="apellido" id="apellido" value="<?php echo $usuarios['apellido'] ?>">
</div>

<div>
<label class="labelaltausu" for="">Email :</label>
<input class="inputalta" type="email" name="email" id="email" value="<?php echo $usuarios['email'] ?>">
</div>
<div>
<label class="labelaltausu" for="password">Contraseña :</label>
<input class="inputalta" type="password" name="contraseña" id="contraseña" value="<?php echo $usuarios['contraseña'] ?>">
</div>
<div>
<label class="labelaltausu" for="fecha">Fecha nacimiento :</label>
<input class="inputalta" type="date" name="fecha" id="fecha" value="<?php echo $usuarios['fecha_nacimiento'] ?>">
</div>
<div>
<input type="hidden" name="id_usuario" value="<?php echo $id_usu ?>" >
<input type="submit" value="Modificar" id="Modificar">

</div>

</form>

<div id="divspan">
    <span id="modiusuconfirm"> </span>
</div>
</section>
<?php
include "footer.php"
?>
</body>
<script src="./js/altausuario.js"></script>
<script src="./js/efectos.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</html>
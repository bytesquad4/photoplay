<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php";
if ($_SESSION['id_usuario']==null) {
header('location:index.php');
}
?>
</header>

<section id="secaltapreguntas">
    <h1>Formulario modificación preguntas</h1>
<form action="" method="post" enctype="multipart/form-data" id="formupdate">
<div class="divpreguntas">
<label class="labelaltapre" for="">Temática</label>
<select class="inputpre" name="tematica" id="tematica">
    <option value="">Elija temática</option>
    <option value="FOL">FOL</option>
    <option value="Inglés">Inglés</option>
</select>
</div>
<div class="divpreguntas">
<label class="labelaltapre" for="">Pregunta</label>
<input class="inputpre" type="text" name="pregunta" id="pregunta">
</div>
<div class="divpreguntas">
<label class="labelaltapre" for="">Respuesta correcta</label>
<input class="inputpre" type="text" name="respuesta_correcta" id="respuesta_correcta">
</div>
<div class="incorrec">
<label class="labelaltapre" for="">Respuestas incorrectas</label> <br>
<label class="labelaltapre" for="">(1)</label>
<input  type="text" name="respuestas_incorrectas" id="incorrecta[]" class="respuestas_incorrectas inputpre"> <br><br>
<label class="labelaltapre" for="">(2)</label>
<input  type="text" name="respuestas_incorrectas" id="incorrecta[]" class="respuestas_incorrectas inputpre"> <br><br>
<label class="labelaltapre" for="">(3)</label>
<input  type="text" name="respuestas_incorrectas" id="incorrecta[]" class="respuestas_incorrectas inputpre">
</div>
<div id="updateimagen" class="divpreguntas">
<label class="labelaltapre" for="">Imagen</label>
<img src="" alt="" id="respuesta">
</div>
<div class="divpreguntas">
<label class="labelaltapre" for="">Elegir nueva imagen</label>
<input class="inputfile" type="file" name="imagen" id="imagen" class="imagen">
</div>
<div class="divpreguntas">
<label class="labelaltapre" for="">Dificultad</label> 
<input class="inputpre" type="text" name="dificultad" id="dificultad" placeholder="La dificultad debe ser entre 1 y 3"> 
<span id="dificul"></span>
</div>
<div class="divpreguntas">
<label class="labelaltapre" for="">Categoria</label>
<select class="inputpre" name="categoria" id="categoria">
    
</select>
</div>

<div class="divpreguntas">
<label class="labelaltapre expli" for="">Explicación</label>
<textarea name="explicacion" id="explicacion" cols="30" rows="10"></textarea>
</div>

<div id="submitpre">
<input type="submit" value="Modificar" id="modificar">
</div>
</form>
<div id="avisopre">
    <span class="spanpre"></span>
</div>
</section>
<?php
include "footer.php"
?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>

<script src="./js/modificarpregunta.js"></script>
</html>
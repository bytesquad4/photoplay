$(document).ready(function () {
    var usuario1
    var contraseña = false

    $("#usuario").focusout(function () {

        var usuario = $(this).val().trim();

        if (usuario.length > 0) {
            $.ajax(
                {
                    data: { Usuario: usuario, function: "usuario" },
                    url: 'http://127.0.0.1/photoplay/servicios.php',
                    type: 'get',
                    success: function (response) {
                        var peticion = response;

                        if (usuario === peticion) {
                            $("#spanusuario").html("<span>Usuario no válido</span>").css('color', 'red')
                            usuario1 = false;


                        } else {
                            $("#spanusuario").html("<span>Usuario válido</span>").css('color', 'green')
                            usuario1 = true;

                        }



                    },
                    error: function (error) {
                        console.log(error);
                    }

                });

        } else {
            $("#nombre").html(" ");
            
            usuario1 = false;

        }
    });

    $("#contraseña2").keyup(function () {
        
        if ($("#contraseña").val().trim().length > 0) {
         console.log($("#contraseña").val())
         if ($("#contraseña").val().trim() == $("#contraseña2").val().trim()) {
             $("#spancontraseña").html("<span>coinciden</span>").css('color','green')
             contraseña =true;
             
         }else{
             $("#spancontraseña").html("<span>No coinciden</span>").css('color','red')
             contraseña =false;
         }
 
        }else{
         
         contraseña =false;
        }
 });





    $('#formaltausu').submit(function (event) {
        var enviar = true
        event.preventDefault();
        $('#altausuconfirm').html("")



        var camposVacios = [];
        $("#formaltausu .inputalta ").each(function () {
            $('#altausuconfirm').html("")
            var valor = $(this).val();

            if (valor === "") {
                camposVacios.push($(this).attr("name"));
                $(this).addClass("inputvacio");

                enviar = false;
            } else {
                $(this).removeClass("inputvacio")
                
            }

        });

        if (enviar && usuario1 && contraseña) {
            this.submit()
        } else {
            $('#altausuconfirm').append('Los campos deben estar llenos').css('color', 'red')
        }


    });

    $('#formModiusu').submit(function (event) {
        var enviar = true
        event.preventDefault();
        $('#modiusuconfirm').html('')


        var camposVacios = [];
        $("#formModiusu .inputalta ").each(function () {
            $('#modiusuconfirm').html("")
            var valor = $(this).val();

            if (valor === "") {
                camposVacios.push($(this).attr("name"));
                $(this).addClass("inputvacio");

                enviar = false;
            } else {
                $(this).removeClass("inputvacio")
                
            }

        });

        if (enviar) {
           this.submit()
           console.log("true")
        } else{
            console.log("false")
            $('#modiusuconfirm').append('Los campos deben estar llenos').css('color', 'red')
        }


    });






});
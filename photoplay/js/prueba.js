$(document).ready(function() {
    $('#image-form').submit(function(e) {
        e.preventDefault();

        var pregunta = new FormData();
        pregunta.append('pregunta', $('#pregunta').val());
        pregunta.append('respuesta_correcta', $('#respuesta_correcta').val());
        pregunta.append('respuestas_incorrectas', $('#respuestas_incorrectas').val());
        pregunta.append('imagen', $('#imagen').prop('files')[0]);
        pregunta.append('dificultad', $('#dificultad').val());
        pregunta.append('categoria', $('#categoria').val());

        $.ajax({
            url: 'http://192.168.0.91:8080/api/games',
            type: 'POST',
            data: pregunta,
            processData: false,
            contentType: false,
            success: function(data) {
                console.log(data);
                alert('Game added successfully!');
            },
            error: function(xhr, status, error) {
                console.error(error);
                alert('Error adding game');
            }
        });
    });
});

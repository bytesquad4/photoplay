$(document).ready(function () {
    var tematica
    var categoria
    var dificultad
    $('#filtrotematica').change(function () {
        $('#filtrocategoria').html("<option value='-1'>Elija categoria</option>")
        tematica = $(this).val()
        $.ajax({
            url: 'http://192.168.0.158:8081/api/categorias/' + tematica,
            type: 'GET',
            data: {},
            success: function (result) {
                result.data.forEach(function (objeto) {


                    // Acceder a los campos del objeto actual


                    var id = objeto._id;
                    var tematica = objeto.tematica;
                    var categoria = objeto.categoria;


                    $('#filtrocategoria').append(
                        "<option value='" + categoria + "'>" + categoria + "</option>"
                    );
                });

            },
            error: function (xhr, status, error) {

                console.error(error);
            }
        });
    });

    $('#filtrocategoria').change(function () {
        $('#filtrodificultad').html("<option value='-1'>Elija dificultad</option>")
        categoria = $(this).val()

        $.ajax({
            url: 'http://192.168.0.158:8081/api/tematica/' + tematica + "/categoria/" + categoria,

            type: 'GET',
            data: {},
            success: function (result) {
                result.data.forEach(function (objeto) {
                    var id = objeto._id;
                    var dificultad = objeto.dificultad;
                    $('#filtrodificultad').append(
                        "<option value='" + dificultad + "'>" + dificultad + "</option>"
                    );

                });

            },
            error: function (xhr, status, error) {

                console.error(error);
            }
        });
    });

    $('#filtrartags').click(function () {
        $('#tablapreguntas').html("")
        $('#tablapreguntas').append(" <tr>",
            "<th>Temática</th>",
            "<th>Pregunta</th>",
            "<th>Categoria</th>",
            "<th>Dificultad</th>",
            "<th>Modificar</th>",
            "<th>Eliminar</th>",
            "</tr>"
        )
        dificultad = $('#filtrodificultad').val()
        $.ajax({
            url: 'http://192.168.0.158:8081/api/tematica/' + tematica + "/categoria/" + categoria + "/dificultad/" + dificultad,

            type: 'GET',
            data: {},
            success: function (result) {

                result.data.forEach(function (objeto) {
                    var join = objeto.categoria[0];

                    var tematica = objeto.tematica;
                    var id = objeto._id;
                    var pregunta = objeto.pregunta;

                    var dificultad = objeto.dificultad;
                    $('#tablapreguntas').append("<tr class='tabinser' >",
                        "<td '>" + tematica + "</td>",
                        "<td class='texto'>" + pregunta + "</td>",
                        "<td>" + join.categoria + "</td>",
                        "<td>" + dificultad + "</td>",
                        "<td><a  href='modipreguntas.php?pregunta=" + id + "'><img src='./img/pencil-square.svg' alt=''></a></td>",
                        "<td><a class='eliminar' href='?pregunta=" + id + "'><img src='./img/trash3.svg' alt=''></a></td>",
                        "</tr>"


                    )




                });

            },
            error: function (xhr, status, error) {

                console.error(error);
            }
        });
    });

});

  $(document).ready(function() {
    $(document).on('click', '.eliminar', function(event) {
      event.preventDefault();
      
  
      swal({
        title: "¿Desea eliminar el contenido?",
        text: "Una vez eliminado no podrás recuperarlo",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          swal("Tu archivo ha sido eliminado correctamente", {
            icon: "success",
          });
           window.location.href = $(this).attr('href');
        } else {
     
        }
      });
    });



    var urlParams = new URLSearchParams(window.location.search);
    var preguntaId = urlParams.get('pregunta');
    if (preguntaId != null) {
      $.ajax({
        url: 'http://192.168.0.158:8081/api/games/' + preguntaId, 
        type: 'DELETE',
        success: function(response) {

          console.log(response.message); 
         
          window.location.href = 'consultarpre.php';
        },
        error: function(xhr, status, error) {
         
          console.error(error); 
        }
      });
      
    }




  });
  
  
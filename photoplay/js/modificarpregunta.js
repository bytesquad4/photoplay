$(document).ready(function () {

    var difi = true
    var urlParams = new URLSearchParams(window.location.search);
    var preguntaId = urlParams.get('pregunta');
    var categoriaselec;
    var tematica;



    $.ajax({
        url: 'http://192.168.0.158:8081/api/games/' + preguntaId,
        type: 'GET',
        data: {},
        success: function (response) {

            tematica = response.data.tematica
            var pregunta = response.data.pregunta;
            var respuestaCorrecta = response.data.respuesta_correcta;
            var respuestasIncorrectas = response.data.respuestas_incorrectas;
            var imagen = response.data.imagen;
            var dificultad = response.data.dificultad;
            categoriaselec = response.data.id_categoria;
            var explicacion = response.data.explicacion;

            var imagenmuestra = $("#respuesta");


            imagenmuestra.attr("src", "./images/" + imagen);


            $('#tematica option').filter(function () {
                return $(this).val() === tematica;
            }).prop('selected', true);
            $('#pregunta').val(pregunta);
            $('#respuesta_correcta').val(respuestaCorrecta);
            $('#incorrecta\\[\\]').each(function (index) {
                $(this).val(respuestasIncorrectas[index]);
            });

            $('#dificultad').val(dificultad);

            $('#explicacion').val(explicacion);

            $.ajax({
                url: 'http://192.168.0.158:8081/api/categorias/' + tematica,
                type: 'GET',
                data: {},
                success: function (result) {
                    result.data.forEach(function (objeto) {

                        var id = objeto._id;

                        var categoria = objeto.categoria;


                        $('#categoria').append(
                            "<option value='" + id + "'>" + categoria + "</option>"
                        );
                    });
                    $('#categoria option').filter(function () {
                        return $(this).val() === categoriaselec;
                    }).prop('selected', true);

                },
                error: function (xhr, status, error) {

                    console.error(error);
                }
            });
        },
        error: function (xhr, status, error) {
            console.error(error);
        }


    });

    $('#tematica').change(function () {
        $('#categoria').html("")
        tematica = $(this).val()
        console.log(tematica)
        $.ajax({
            url: 'http://192.168.0.158:8081/api/categorias/' + tematica,
            type: 'GET',
            data: {},
            success: function (result) {
                result.data.forEach(function (objeto) {


                    var id = objeto._id;

                    var categoria = objeto.categoria;


                    $('#categoria').append(
                        "<option value='" + id + "'>" + categoria + "</option>"
                    );
                });
                $('#categoria option').filter(function () {
                    return $(this).val() === categoriaselec;
                }).prop('selected', true);

            },
            error: function (xhr, status, error) {

                console.error(error);
            }
        });
    })








    $('#formupdate').submit(function (event) {
        var enviar = true;
        $('#dificul').html("")
        $('.spanpre').html('')
        var num = $('#dificultad').val()
        if (num > 0 && num < 4) {
            difi = true


        } else {
            $('#dificul').append("Solo valores entre 1 y 3").css('color', 'red')
            $('#dificulad').addClass("inputvacio")
            difi = false
        }


        event.preventDefault();





        var camposVacios = [];

        $("#formupdate .inputpre, #explicacion").each(function () {
            var valor = $(this).val();

            if (valor === "") {
                camposVacios.push($(this).attr("name"));
                $(this).addClass("inputvacio");

                enviar = false;
            } else {
                $(this).removeClass("inputvacio")

            }

        });

        if (enviar) {

            if (difi) {

                var formData = new FormData(this);
                for (var pair of formData.entries()) {
                    console.log(pair[0] + ': ' + pair[1]);
                }
                $.ajax({
                    url: 'http://192.168.0.158:8081/api/games/' + preguntaId,
                    type: 'PUT',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        window.location.href = 'consultarpre.php';

                        console.log(response.message);

                    },
                    error: function (xhr, status, error) {

                        console.error(error);
                    }
                });

            } else {
                $('#dificulad').addClass("inputvacio")
                $('#dificul').append("Solo valores entre 1 y 3").css('color', 'red')
            }



        } else {
            $('.spanpre').append('Los campos deben estar llenos').css('color', 'red');
        }

    });








});
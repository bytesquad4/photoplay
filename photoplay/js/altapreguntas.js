$(document).ready(function () {
  var difi = true
  $('#tematica').change(function () {
    var tematica = $(this).val()
    $('#categoria').html("")
    console.log(tematica)
    
    $.ajax({
      url: 'http://192.168.0.158:8081/api/categorias/' + tematica,
      type: 'GET',
      data: {},
      success: function (result) {
        result.data.forEach(function (objeto) {
          // Acceder a los campos del objeto actual


          var id = objeto._id;
          var tematica = objeto.tematica;
          var categoria = objeto.categoria;


          $('#categoria').append(
            "<option value='" + id + "'>" + categoria + "</option>"
          );



        });

      },
      error: function (xhr, status, error) {

        console.error(error);
      }
    });
  });



  $('#formaltapre').submit(function (event) {
    var enviar = true;
    $('#dificul').html("")
    var num = $('#dificultad').val()
    if (num > 0 && num < 4) {
      difi = true

    } else {
      $('#dificul').append("Solo valores entre 1 y 3").css('color', 'red')
      $('#dificulad').addClass("inputvacio")
      difi = false

    }
    $('.spanpre').html('')
    event.preventDefault();
    

    var camposVacios = [];

    $("#formaltapre .inputpre, #explicacion").each(function () {
      var valor = $(this).val();

      if (valor === "") {
        camposVacios.push($(this).attr("name"));
        $(this).addClass("inputvacio");

        enviar = false;
      } else {
        $(this).removeClass("inputvacio")
      }
     
    });

    if (enviar) {

      if (difi) {
        var pregunta = new FormData();
        pregunta.append('tematica', $('#tematica').val());
        pregunta.append('pregunta', $('#pregunta').val());
        pregunta.append('respuesta_correcta', $('#respuesta_correcta').val());
        pregunta.append('imagen', $('#imagen').prop('files')[0]);
        pregunta.append('dificultad', $('#dificultad').val());
        pregunta.append('id_categoria', $('#categoria').val());
        pregunta.append('explicacion', $('#explicacion').val());


        var respuestasIncorrectas = [];

        $('.respuestas_incorrectas').each(function () {
          respuestasIncorrectas.push($(this).val());
          console.log($(this).val())
        });



        pregunta.append('respuestas_incorrectas', JSON.stringify(respuestasIncorrectas));





        $.ajax({
          url: 'http://192.168.0.158:8081/api/games/',
          type: 'POST',
          data: pregunta,
          processData: false,
          contentType: false,
          success: function (response) {
            window.location.href = 'consultarpre.php';
            console.log(response);
          },
          error: function (xhr, status, error) {

            console.error(error);
          }
        });
      }else{
        $('#dificulad').addClass("inputvacio")
      }

    } else {
      $('.spanpre').append('Los campos deben estar llenos').css('color', 'red');
    }




  });
});

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php";
if ($_SESSION['id_usuario']==null) {
header('location:index.php');
}
?>
</header>

<section id="secmodiusu">
    <h1>Modificación y eliminación de usuarios</h1>
<table id="tablamodiusu">
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Usuario</th>
                <th>Modificaciones</th>
                <th>Bajas</th>
            </tr>
            <?php
            include 'bbdd.php';

             // Número de registros por página
             $registrosPorPagina = 5;

             // Página actual obtenida del parámetro en la URL
             $paginaActual = isset($_GET['pagina']) ? $_GET['pagina'] : 1;
 
             // Obtiene los usuarios para la página actual
             $usuarios = mostrarUsuariosPaginados($paginaActual, $registrosPorPagina);

             foreach ($usuarios as $usuario) {
              echo '
                  <tr>
                      <td>'.$usuario['nombre'].'</td>
                      <td>'.$usuario['apellido'].'</td>
                      <td>'.$usuario['user'].'</td>
                      <td><a href="modificacion.php?id_usuario='.$usuario['id_usuario'].'"><img src="./img/pencil-square.svg" alt=""></a></td>
                      <td><a class="eliminar" href="eliminarusu.php?id_usuario='.$usuario['id_usuario'].'"><img src="./img/trash3.svg" alt=""></a></td>
                  </tr>

            <section id="modiybaja">
            <div>
               <p class="titulo">Nombre</p>
               <p>'.$usuario['nombre'].'</p>
            </div>
            <div>
               <p class="titulo">Apellido</p>
               <p>'.$usuario['apellido'].'</p>
            </div>
            <div>
               <p class="titulo">Usuario</p>
               <p>'.$usuario['user'].'</p>
            </div>
            <div>
               <p class="titulo">Modificar</p>
               <a href="modificacion.php?id_usuario='.$usuario['id_usuario'].'"><img src="./img/pencil-square.svg" alt=""></a>
            </div>
            <div>
               <p class="titulo">Eliminar</p>
               <a class="eliminar" href="eliminarusu.php?id_usuario='.$usuario['id_usuario'].'"><img src="./img/trash3.svg" alt=""></a>
            </div>
          </section>
              ';
          }

          

          // Obtener el número total de registros
          $totalRegistros = obtenerTotalUsuarios();

          // Calcular el número total de páginas
          $totalPaginas = ceil($totalRegistros / $registrosPorPagina);
       ?>
           
           
        </table>
         <!-- Enlaces de paginación -->
         <div class="paginacion">
            <?php
            for ($i = 1; $i <= $totalPaginas; $i++) {
                echo '<a class="pagi" href="bajasmodi.php?pagina=' . $i . '">' . $i . '</a>';
            }
            ?>
        </div>
</section>
<?php
include "footer.php"
?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
<script src="./js/confirmacion.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</html>
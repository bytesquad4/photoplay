<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php";
if ($_SESSION['id_usuario']==null) {
header('location:index.php');
}
?>
</header>

<section id="secaltausu">
    <h1>Consulta de preguntas</h1>
 
    <div id="consulta">
        <div>
        <label for="">temática</label>
        <select class="consulta" name="filtrotematica" id="filtrotematica">
        <option value="-1">Elija temática</option>
            <option value="FOL">FOL</option>
            <option value="Inglés">Inglés</option>
        </select>
        </div>
        
       <div>
        <label for="">Categoria </label>
        <select class="consulta" name="filtrocategoria" id="filtrocategoria">
        <option value="-1">Elija categoria</option>
        </select>
      </div>
        
        <div class="divdifi">
        <label for="">Dificultad</label>
        <select class="consulta" name="filtrodificultad" id="filtrodificultad">
        <option value="-1">Elija dificultad</option>
        </select>
        </div>
        
         <div class="divbotones">
         <input type="submit" value="Buscar" id="filtrartags">

         <input type="submit" value="Mostrar todo" id="mostrartodo">
         </div>
        
    </div>
<table id="tablapreguntas">
            <tr>
                <th>Temática</th>
                <th>Pregunta</th>
                <th>Categoria</th>
                <th>Dificultad</th>
                <th>Modificar</th>
                <th>Eliminar</th>
            </tr>
       
           
           
        </table>
</section>
<?php
include "footer.php"
?>
</body>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
<script src="./js/confirmacion.js"></script>
<script src="./js/todaspreguntas.js"></script>
<script src="./js/buscador.js"></script>



</html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="./css/login.css">
<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php"
?>
</header>
<div id="divlinea">
<hr id="linea">
</div>
<section id="seclogin">
<form action="iniciosesion.php" method="POST">
<div class="group">
<input type="text" name="usuario" id="" placeholder="Usuario"><span class="highlight"></span><span class="bar"></span>
</div>
<div class="group">
<input type="password" name="password" id="" placeholder="Contraseña"><span class="highlight"></span><span class="bar"></span>
</div>
<input type="submit" class="button buttonBlue" value="Login">
</form>
<div id="divlogin">
    <span id="spanlogin"></span>
</div>
</section>
<div id="divlinea">
<hr id="linea">
</div>
<?php
include "footer.php"
?>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
<script src="./js/login.js"></script>
</html>
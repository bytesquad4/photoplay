<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<link rel="stylesheet" href="./css/style.css">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&display=swap" rel="stylesheet">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/css2?family=Bruno+Ace+SC&family=Dosis:wght@200;500&display=swap" rel="stylesheet">
<title>Photoplay</title>
</head>
<body>
<header>
<?php
include "menu.php"
?>
<div id="imgheader">
<img id="imgheader" src="./img/header.jpeg" alt="">
<div id="txtprincipal">
<p id="txt1">Photoplay_ <br> Game</p>
<p class="gamemulti">Juego multiplataforma</p>
<div>
<img src="./img/computador.png" alt="">
<img src="./img/telefono.png" alt="">
<img src="./img/consola.png" alt="">
</div>
<button class="descargar">Descargar gratis</button>
</div>
</div>
</header>
<div id="divlinea">
<hr id="linea">
</div>
<section id="sobrejuego">
<div class="imgabout">
<img class="ocultar"src="./img/computador.jpg" alt="">
</div>
<div class="txtabout">
<p class="title2sec"> <span class="ph">Photoplay:</span> <br> Aventura de Conocimientos y Seguridad</p>
<p class="parraabout">Photoplay es un emocionante juego que combina diversión y aprendizaje, con el objetivo de transmitir conocimientos sobre informática y tecnología en inglés, al mismo tiempo que crea conciencia y proporciona información sobre Prevención de Riesgos Laborales (PRL) relacionados con trabajos informáticos y primeros auxilios. <br><br>

La dinámica del juego es sencilla pero desafiante. En cada nivel, se mostrará una imagen relacionada con el campo de la informática y la tecnología. Los jugadores tendrán un tiempo limitado para observar detenidamente la imagen y absorber todos los detalles. Una vez que el tiempo se agote, la imagen se ocultará y se presentará una pregunta relacionada con la misma, junto con varias opciones de respuesta. <br><br>
Los jugadores deberán utilizar su conocimiento en informática y tecnología, así como su comprensión de las mejores prácticas de seguridad en el lugar de trabajo, para seleccionar la respuesta correcta. Cada respuesta correcta les otorgará puntos y los acercará a la victoria, mientras que las respuestas incorrectas podrían reducir sus vidas o puntuación. <br><br>

<img id="imgtexto" src="./img/computador.jpg" alt="">
Photoplay también ofrece elementos adicionales para mantener a los jugadores comprometidos y desafiar su conocimiento. Estos elementos incluyen comodines, que les permitirán eliminar opciones incorrectas y aumentar sus posibilidades de responder correctamente. Además, habrá pantallas de bonus ocasionales, donde los jugadores podrán ganar puntos adicionales o vidas extras. <br><br>

Con gráficos atractivos y una interfaz intuitiva, Photoplay garantiza una experiencia de juego envolvente y entretenida. Ya sea que los jugadores estén interesados en mejorar sus conocimientos informáticos o deseen aprender sobre PRL en trabajos informáticos y primeros auxilios, este juego ofrece una manera divertida y educativa de lograrlo. <br><br>

¡Sumérgete en la aventura de conocimientos y seguridad de Photoplay y conviértete en un experto en informática mientras te mantienes seguro en el mundo laboral!
</p>

</div>

</section>
<div id="divlinea">
<hr id="linea">
</div>
<section id="secdescaga">
<h1 id="descargar">Descargar</h1>
<p class="txtdescarga">¡Descarga Photoplay ahora en tu PC, dispositivo móvil o consola y conviértete en un experto en informática mientras te mantienes seguro en el mundo laboral!</p>
<article id="cards">
<div class="card" style="width: 18rem;">
<img src="./img/computador.png" class="card-img-top icon" alt="...">
<div class="card-body">
<h5 class="card-title">Escritorio</h5>
<p class="card-text">Descarga el juego para las plataformass de escritorio.</p>
<a href="https://drive.google.com/drive/folders/1eypl0Jw8-KkR9BojCg_1GsM9i6sGAVVM?usp=share_link" class="btn btn-dark" target="_blank">Descargar</a>
</div>
</div>
<div class="card" style="width: 18rem;">
<img src="./img/telefono.png" class="card-img-top icon" alt="...">
<div class="card-body">
<h5 class="card-title">Dispositivo movil</h5>
<p class="card-text">Descarga el juego para las plataformas moviles.</p>
<a href="https://drive.google.com/drive/folders/1eypl0Jw8-KkR9BojCg_1GsM9i6sGAVVM?usp=share_link" class="btn btn-dark" target="_blank">Descargar</a>
</div>
</div>
<div class="card" style="width: 18rem;">
<img src="./img/consola.png" class="card-img-top icon" alt="...">
<div class="card-body">
<h5 class="card-title">Consola</h5>
<p class="card-text">Descarga el juego para las consolas.</p>
<a href="https://drive.google.com/drive/folders/1eypl0Jw8-KkR9BojCg_1GsM9i6sGAVVM?usp=share_link" class="btn btn-dark" target="_blank">Descargar</a>
</div>
</div>
</article>
</section>
<?php
include "footer.php"
?>
</body>
<script src="./js/boton.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</html>

// gameModel.js

var mongoose = require('mongoose');


var preguntaSchema = mongoose.Schema(
    {
        tematica: String,
        pregunta: String,
        respuesta_correcta: String,
        respuestas_incorrectas: [String],
        imagen: String,
        dificultad: Number,
        id_categoria: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Categoria' 
        },
        explicacion: String
    },
    { collection: 'preguntas' }
);

var Game  = mongoose.model('game', preguntaSchema);

var categoriaSchema = mongoose.Schema({
    tematica:String,
    categoria: String
}, { collection: 'categorias' });

var Categoria = mongoose.model('Categoria', categoriaSchema);

module.exports = {
    Game: Game,
    Categoria: Categoria,
    Funcionget : function(limit)
    {
        var games = Game.find({}).limit(limit);
        return games;
    },
    Funciongetcate : function(limit)
    {
        var categoria = Categoria.find({}).limit(limit);
        return categoria;
    },
    Funcionjoin : function () {
        var preguntas = Game.aggregate([
            {
                $lookup: {
                  from: 'categorias',
                  localField: "id_categoria",
                  foreignField: "_id",
                  as: "categoria"
                }
              }
    ])
        return preguntas;
        
    },
    Funcioncatefiltro : function(tematica, ){
       var categorias = Categoria.find({"tematica":tematica})
        return categorias
    },
    Funcioncategoria : function(categoria, ){
        var categorias = Categoria.find({"categoria":categoria})
         return categorias
     },
     Funcionfiltro: function (tematica, categoria) {
        var preguntas = Game.aggregate([
          {
            $lookup: {
              from: 'categorias',
              localField: "id_categoria",
              foreignField: "_id",
              as: "categoria"
            }
          },
          {
            $match: {
              "tematica": tematica,
              "categoria.categoria": categoria
            }
          },
          {
            $group: {
              _id: "$dificultad"
            }
          },
          {
            $project: {
              _id: 0,
              dificultad: "$_id"
            }
          }
        ]);
        
        return preguntas;
      }
      ,
      Funcionfiltrofinal : function (tematica, categoria, dificultad) {
        var preguntas = Game.aggregate([
            {
                $lookup: {
                  from: 'categorias',
                  localField: "id_categoria",
                  foreignField: "_id",
                  as: "categoria"
                }
              }, {$match :{"dificultad": dificultad,
                "tematica": tematica,
              "categoria.categoria": categoria
            }}
    ])
        return preguntas}
      ,
};





















































//Filename: api-routes.js

//Initialize express router
let router = require('express').Router();
const upload = require('./upload')



//Default API response
router.get('/', function (req, res) {
    res.json(
        {
            status: 'API is working',
            message: 'Bienvenidos al mejor WS mundial'
        }
    );
});

//Import Game Controller
var gameController = require('./gameController');

//preguntas routes
router.route('/preguntas/tema/:tema/:dificultad')
    .get(gameController.dificultad);

router.route('/games')
    .get(gameController.index)
    .post(upload.single('imagen'), gameController.newP);

router.route('/games/:id')
    .get(gameController.getByid)
    .put(upload.single('imagen'), gameController.updateP)
    .delete(gameController.deleteP);

//categorias routes

router.route('/tematica/:tematica/categoria/:categoria/dificultad/:dificultad')
    .get(gameController.filtrofinal)

router.route('/categorias')
    .get(gameController.indexCategoria)
    .post(gameController.newCategoria);

router.route('/categorias/cate/:categoria')
    .get(gameController.categoria);

router.route('/categorias/:tematica')
    .get(gameController.filtrocate)

router.route('/tematica/:tematica/categoria/:categoria')
    .get(gameController.filtroprefinal)


router.route('/tematica/:tematica/categoria/:categoria/dificultad/:dificultad')
    .get(gameController.filtrofinal)





module.exports = router;
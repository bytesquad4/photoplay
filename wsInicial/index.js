// Filename: index.js

//Import body parser
let bodyParser = require('body-parser');

//Import Mongoose
let mongoose = require('mongoose');

//Importar express
let express = require('express');

//Import router
let apiRoutes = require('./api-routes');

//Initialize app
let app = express();

const cors = require('cors');

app.use(cors()); 

//Configure body parser for POST requests
app.use(bodyParser.urlencoded(
    {
        extended: true
    }
));
app.use(bodyParser.json());

app.use(function(req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');

    next();

  });


//Connect Mongo

mongoose.connect('mongodb://localhost:27017/photoplay', {useNewUrlParser:true, useUnifiedTopology:true});

var db = mongoose.connection;


if(!db)
{
    console.log('Error connecting db');
} else
{
    console.log('Db connected succesfully');
}

//Setup server port
var port = process.env.PORT || 8081;

//Default url
app.get('/', (req, res) => res.send('El mejor WS de la historia'));

app.use('/api', apiRoutes);

//Launch app
app.listen(port, function()
{
    console.log("Running server on: " + port);
});












/**/
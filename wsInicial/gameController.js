// gameController.js
const path = require('path');


//Import game model
const { Game, Categoria, Funcionget, Funciongetcate, Funcionjoin, Funcioncatefiltro, Funcionfiltro, Funcionfiltrofinal, Funcioncategoria} = require('./gameModel');



//Handle index actions
exports.index = function(req, res, next)
{
  Funcionjoin().then(function(games)
    {
        res.json({
            status: "success",
            message: "Preguntas obtenidas correctamente",
            data: games
        });
    });
};
exports.dificultad = function(req, res, next)
{var tema = req.params.tema
  var dificultad = req.params.dificultad
  Game.find({"tematica": tema,"dificultad":dificultad}).then(function(games)
    {
        res.json({
            status: "success",
            message: "Preguntas por tema y dificultad obtenidas correctamente",
            data: games
        });
    });
};

exports.getByid = function(req, res, next)
{   var id = req.params.id
    Game.findById(id).then(function(pregunta)
    {
        res.json({
            status: "success",
            message: "pregunta obtenida correctamente",
            data: pregunta
        });
    });
};

exports.newP = function(req, res, next) {
    // Crear un objeto Game con los datos de la pregunta
    const respuestas_incorrectas = JSON.parse(req.body.respuestas_incorrectas)
    
    const game = new Game({
      tematica: req.body.tematica,
      pregunta: req.body.pregunta,
      respuesta_correcta: req.body.respuesta_correcta,
      respuestas_incorrectas: respuestas_incorrectas,
      dificultad: req.body.dificultad,
      id_categoria : req.body.id_categoria,
      explicacion: req.body.explicacion
    });
  
  if(req.file) {
    const fileName = path.basename(req.file.path);
    game.imagen = fileName;
    
  }
    game.save().then(response => {
        res.json({
            message: 'Nueva pregunta creada'
            
        })
    }).catch(error => {
        res.json({
            message: 'Ocurrió un error'
        })
    })
};

exports.updateP = function(req, res, next) {
    const id = req.params.id;
  
    // Create an object with the updated fields
    const update = {};
    if (req.body.explicacion) update.explicacion = req.body.explicacion;
    if (req.body.tematica) update.tematica = req.body.tematica;
    if (req.body.pregunta) update.pregunta = req.body.pregunta;
    if (req.body.respuesta_correcta) update.respuesta_correcta = req.body.respuesta_correcta;
    if (req.body.respuestas_incorrectas) update.respuestas_incorrectas = req.body.respuestas_incorrectas;
    if (req.body.dificultad) update.dificultad = req.body.dificultad;
    if (req.body.categoria) update.id_categoria = req.body.categoria;
    if (req.file) update.imagen = path.basename(req.file.path);
  
    Game.findByIdAndUpdate(id, update, {new: true})
      .then(game => {
        if (!game) {
          return res.status(404).json({
            message: 'Game not found'
          });
        }
        res.json({
          message: 'Game updated successfully',
          game: game
        });
      })
      .catch(error => {
        res.json({
          message: 'Error updating game'
        });
      });
  };

  exports.deleteP = function(req, res, next) {
    const id = req.params.id;
  
    Game.findByIdAndDelete(id)
      .then(game => {
        if (!game) {
          return res.status(404).json({
            message: 'Game not found'
          });
        }
        res.json({
          message: 'Game deleted successfully'
        });
      })
      .catch(error => {
        res.json({
          message: 'Error deleting game'
        });
      });
  };
  
  

  
  


  

exports.view = function(req, res)
{
    Game.findById(req.params.game_id).then(function(game, err)
    {
        if(err)
        {
            res.json({message: err});
        }

        res.json({
            message: 'Game details',
            data: game
            }
        );
    });
}

exports.update = function(req, res)
{
    Game.findById(req.params.game_id).then(function(game, err)
    {
        if(err)
        {
            res.send(err);
        }

        game.name = req.body.name ? req.body.name : game.name;
        game.description = req.body.description ? req.body.description: game.description;
        game.type = req.body.type ? req.body.type : game.type;
        game.available = req.body.available ? req.body.available: game.available;
        game.price = req.body.price ? req.body.price : game.price;
        game.release_date = req.body.release_date ? req.body.release_date : game.release_date;
        game.platforms = req.body.platforms ? req.body.platforms : game.platforms;

        game.save().then(function(juego, err)
        {
            if(err)
            {
                res.send(err);
            } else
            {
                res.json({
                    message:'Game updated',
                    data:juego
                });
            }

            
        });
    });
}

exports.delete = function(req, res)
{
    Game.deleteOne({_id: req.params.game_id}).then(function(game, err)
    {
        if(err)
        {
            res.send(err);
        } else
        {
            res.json({
                status: 'Success',
                message: 'Game deleted'
            });
        }
    });
}

// funciones categoria

exports.categoria = function(req, res, next)
{   var categoria = req.params.categoria
  Funcioncategoria(categoria).then(function(pregunta)
    {
        res.json({
            status: "success",
            message: "categoría obtenida correctamente",
            data: pregunta
        });
    });
};

exports.indexCategoria = function(req, res, next)
{
  Funciongetcate().then(function(categoria)
    {
        res.json({
            status: "success",
            message: "Categorías obtenidos correctamente",
            data: categoria
        });
    });
};


exports.newCategoria = function(req, res, next) {
  
  const categoria = new Categoria({
    tematica: req.body.tematica,
    categoria: req.body.categoria,
    
  });


categoria.save().then(response => {
      res.json({
          message: 'Nueva categoría creada'
      })
  }).catch(error => {
      res.json({
          message: 'Ocurrió un error'
      })
  })
};

exports.filtrocate = function(req, res)
{
  var tematica = req.params.tematica
  Funcioncatefiltro(tematica).then(function(categoria)
    {
        res.json({
            status: "success",
            message: "Preguntas obtenidas correctamente",
            data: categoria
        });
    });
};


exports.filtroprefinal = function(req, res)
{
  var tematica = req.params.tematica
  var categoria = req.params.categoria
  Funcionfiltro(tematica, categoria).then(function(categoria)
    {
        res.json({
            status: "success",
            message: "Pregunta obtenida correctamente",
            data: categoria
        });
    });
};



exports.filtrofinal = function(req, res)
{
  var tematica = req.params.tematica
  var categoria = req.params.categoria
  var  dificultad = parseInt(req.params.dificultad)

  Funcionfiltrofinal(tematica, categoria, dificultad).then(function(categoria)
    {
        res.json({
            status: "success",
            message: "Pregunta final obtenida correctamente",
            data: categoria
        });
    });
};